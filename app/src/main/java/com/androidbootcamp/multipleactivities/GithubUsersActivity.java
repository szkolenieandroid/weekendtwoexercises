package com.androidbootcamp.multipleactivities;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.JsonReader;
import android.widget.ListView;

import com.androidbootcamp.multipleactivities.model.GitHubUser;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class GithubUsersActivity extends Activity {

    private GithubUserAdapter githubUserAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        initListView();
        requestApi();
    }

    private void initListView() {
        ListView listView = (ListView) findViewById(R.id.usersListView);
        githubUserAdapter = new GithubUserAdapter(this, null);
        listView.setAdapter(githubUserAdapter);
    }


    private void requestApi() {
        ApiRequestAsyncTask asyncTask = new ApiRequestAsyncTask();
        asyncTask.requestUrl = "https://api.github.com/users/JakeWharton/followers";
        asyncTask.execute();

    }

    class ApiRequestAsyncTask extends AsyncTask<Void, Void, Void> {

        String requestUrl;
        private List<GitHubUser> gitHubUsers;
        @Override
        protected Void doInBackground(Void... params) {
            HttpClient httpClient = new DefaultHttpClient();
            try {
                HttpResponse response = httpClient.execute(new HttpGet(requestUrl));
                gitHubUsers = parseGitHubUserList(response.getEntity().getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            presentGithubUsers(gitHubUsers);
        }
    }

    private void presentGithubUsers(List<GitHubUser> gitHubUsers) {
        githubUserAdapter.setGitHubUsers(gitHubUsers);
        githubUserAdapter.notifyDataSetChanged();
    }

    private List<GitHubUser> parseGitHubUserList(InputStream content) {
        Gson gson = new Gson();
        Type collectionType = new TypeToken<List<GitHubUser>>(){}.getType();
        Reader jsonReader = new InputStreamReader(content);
        List<GitHubUser> result = gson.fromJson(jsonReader, collectionType);
        return result;
    }

}
