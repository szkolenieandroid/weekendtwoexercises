package com.androidbootcamp.multipleactivities.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dmitry on 06/11/14.
 */
public class GitHubUser {

    @SerializedName("login")
    private String name;
    @SerializedName("avatar_url")
    private String avatarUrl;

    public String getName() {
        return name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }
}
